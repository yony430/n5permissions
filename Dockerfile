# Paso 1: Construir la aplicación
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src

# N5Permissions.Domain
COPY ["N5Permissions.Domain/N5Permissions.Domain.csproj", "N5Permissions.Domain/"]
RUN dotnet restore "N5Permissions.Domain/N5Permissions.Domain.csproj"

# N5Permissions.Infrastructure
COPY ["N5Permissions.Infrastructure/N5Permissions.Infrastructure.csproj", "N5Permissions.Infrastructure/"]
RUN dotnet restore "N5Permissions.Infrastructure/N5Permissions.Infrastructure.csproj"

# N5Permissions.Business
COPY ["N5Permissions.Business/N5Permissions.Business.csproj", "N5Permissions.Business/"]
RUN dotnet restore "N5Permissions.Business/N5Permissions.Business.csproj"

# N5Permissions
COPY ["N5Permissions/N5Permissions.csproj", "N5Permissions/"]
RUN dotnet restore "N5Permissions/N5Permissions.csproj"

COPY . .

# Compila N5Permissions.Domain
WORKDIR "/src/N5Permissions.Domain"
RUN dotnet build -c Release -o /app/build

# Compila N5Permissions.Infrastructure
WORKDIR "/src/N5Permissions.Infrastructure"
RUN dotnet build -c Release -o /app/build

# Compila N5Permissions.Business
WORKDIR "/src/N5Permissions.Business"
RUN dotnet build -c Release -o /app/build

# Compila N5Permissions
WORKDIR "/src/N5Permissions"
RUN dotnet build -c Release -o /app/build

# Paso 2: Publicando la aplicación
FROM build AS publish
RUN dotnet publish "N5Permissions.csproj" -c Release -o /app/publish /p:UseAppHost=false

# Paso 3: Crear la imagen
FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS final
WORKDIR /app
EXPOSE 80
ENV ASPNETCORE_URLS=http://+:80
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "N5Permissions.dll"]
