﻿using Autofac;
using N5Permissions.Business.Services;
using N5Permissions.Business.Services.Implementations;
using N5Permissions.Domain.Contracts;
using N5Permissions.Infrastructure.Repositories;

namespace N5Permissions.IoCContainer
{
    public static class IoCServices
    {
        public static ContainerBuilder BuildContext(ContainerBuilder builder, IConfiguration configuration)
        {
            builder.Register((context, parameters) => new ElasticSearch(
                configuration["ElasticSearch:Url"]
                )).As<IElasticSearch>().SingleInstance();

            builder.Register((context, parameters) => new Kafka(
                configuration["kakfa:Url"]
                )).As<IKafka>().SingleInstance();

            builder.Register((context, parameters) =>
             new PermissionService(
                 context.Resolve<IDataBaseContext>(),
                 context.Resolve<IElasticSearch>(),
                 context.Resolve<IKafka>()
           )).As<IPermissionService>().SingleInstance();

            return builder;
        }
    }
}
