﻿using Autofac.Extensions.DependencyInjection;

namespace N5Permissions
{
    public class Program
    {

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            var pathToContentRoot = AppDomain.CurrentDomain.BaseDirectory;
            var configurationRoot = new ConfigurationBuilder()
                .SetBasePath(pathToContentRoot)
                .AddJsonFile($"appsettings.Development.json", optional: false, reloadOnChange: true)
                .Build();

            return Host.CreateDefaultBuilder(args)
                    .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                    .ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        config.AddConfiguration(configurationRoot);
                    })
                    .ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder.UseStartup<Startup>();
                    });
        }
    }
}
