﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using N5Permissions.Business.Services;
using N5Permissions.Domain.Models;
using N5Permissions.Models.Request;
using N5Permissions.Models.Validator;

namespace N5Permissions.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PermissionsController : Controller
    {

        private readonly IPermissionService _permissionService;

        public PermissionsController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }

        [HttpPost(Name = "RequestPermission")]
        public async Task<IActionResult> Request([FromBody] PermissionDTO permission)
        {
            var validationResult = new PermissionDTOValidation().Validate(permission);
            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors.Select(e => e.ErrorMessage));
            }
            var created = await _permissionService.RequestAsync(new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<PermissionDTO, Permission>())).Map<Permission>(permission));
            return Ok(created);
        }

        [HttpPut("{id}", Name = "ModifyPermission")]
        public async Task<IActionResult> Modify(int id, [FromBody] PermissionDTO permission)
        {
            var validationResult = new PermissionDTOValidation().Validate(permission);
            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors.Select(e => e.ErrorMessage));
            }

            var edit = await _permissionService.ModifyAsync(id, new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<PermissionDTO, Permission>())).Map<Permission>(permission));
            return Ok(edit);
        }

        [HttpGet(Name = "GetPermissions")]
        public async Task<IActionResult> Get()
        {
            var permissions = await _permissionService.GetAsync();
            return Ok(permissions);
        }
    }
}
