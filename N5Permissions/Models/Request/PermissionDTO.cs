﻿namespace N5Permissions.Models.Request
{
    public class PermissionDTO
    {
        public string EmployeeForename { get; set; }
        public string EmployeeSurname { get; set; }
        public int PermissionTypeId { get; set; }
    }
}
