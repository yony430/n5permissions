﻿using FluentValidation;
using N5Permissions.Models.Request;

namespace N5Permissions.Models.Validator
{
    public class PermissionDTOValidation : AbstractValidator<PermissionDTO>
    {
        public PermissionDTOValidation() {
            RuleFor(x => x.EmployeeForename)
                    .NotEmpty().WithMessage("Employee forename is required.");

            RuleFor(x => x.EmployeeSurname)
                    .NotEmpty().WithMessage("Employee surname is required.");

            RuleFor(x => x.PermissionTypeId)
                .NotEmpty().WithMessage("permissionTypeId is required.")
                .GreaterThan(0).WithMessage("permissionTypeId must be greater than 0.");
        }
    }
}
