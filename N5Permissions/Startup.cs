using Serilog;
using N5Permissions.Serilog;
using N5Permissions.IoCContainer;
using N5Permissions.Domain.Contracts;
using N5Permissions.Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace N5Permissions
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddControllers(options =>
            {
                options.Filters.Add<ExceptionFilter>();
            });
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddTransient<IDataBaseContext, DataBaseContext>();
            services.AddDbContext<DataBaseContext>(o => o.UseSqlServer(Configuration["SQLServer:ConnectionString"]));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            Task.Run(async () =>
            {
                using (var scope = app.ApplicationServices.CreateScope())
                {
                    var serviceProvider = scope.ServiceProvider;
                    await MigrateDatabaseAsync(serviceProvider);
                }
            }).Wait();
        }

        public void ConfigureContainer(Autofac.ContainerBuilder builder)
        {
            IoCServices.BuildContext(builder, Configuration);
            ConfigureLogging(Configuration);
        }

        private async Task MigrateDatabaseAsync(IServiceProvider provider)
        {
            try
            {
                var context = provider.GetService<DataBaseContext>();
                context.Database.GetDbConnection();
                await context.Database.MigrateAsync();
            }
            catch (Exception ex)
            {
                Console.Write($"Error MigrateDatabaseAsync: ---> {ex.Message}");
            }
        }

        private static void ConfigureLogging(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration()
            .MinimumLevel
            .ControlledBy(
                new EnvironmentVariableLoggingLevelSwitch(configuration["Logging:LogLevel:Default"]))
            .Enrich.With(new ThreadIdEnricher())
            .WriteTo.Console(outputTemplate: "{Timestamp:HH:mm} [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}")
            .CreateLogger();
        }
    }
}