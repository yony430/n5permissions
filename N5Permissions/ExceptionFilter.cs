﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using N5Permissions.Domain.Exception;
using Newtonsoft.Json;
using Serilog;

namespace N5Permissions
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private const string _errorResponseMessage = "Error while processing your request";

        public override void OnException(ExceptionContext context)
        {

            Log.Error(context.Exception.Message);

            context.Result = context.Exception switch
            {
                NotPermissionTypeFoundException or NotPermissionFoundException => new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { message = context.Exception.Message }),
                    StatusCode = 404
                },
                KafkaException => new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { message = context.Exception.Message }),
                    StatusCode = 500
                },
                _ => new ContentResult
                {
                    Content = $"{_errorResponseMessage} -> {context.Exception.Message}",
                    StatusCode = 500
                },
            };
        }
    }
}
