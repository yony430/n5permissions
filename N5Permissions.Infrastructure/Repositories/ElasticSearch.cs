﻿using N5Permissions.Domain.Contracts;
using N5Permissions.Domain.Models;
using Nest;

namespace N5Permissions.Infrastructure.Repositories
{
    public class ElasticSearch : IElasticSearch
    {

        private readonly ElasticClient _elasticClient;

        public ElasticSearch(string url)
        {
            _elasticClient = new ElasticClient(new ConnectionSettings(new Uri(url)).DefaultIndex("permissions"));
        }

        public async Task<bool> CreateIndexAsync(Permission permission)
        {
            var response = await _elasticClient.IndexDocumentAsync(permission);
            return response.IsValid;
        }

        public async Task<IEnumerable<Permission>> GetAsync()
        {
            ISearchResponse<Permission> t = await _elasticClient.SearchAsync<Permission>();
            return t.Documents;
        }

        public async Task<bool> ModifyIndexAsync(Permission permission)
        {
            var response = await _elasticClient.UpdateAsync(
                new DocumentPath<Permission>(permission), u => u.Doc(permission).RetryOnConflict(2)
                );
            return response.IsValid;
        }
    }
}
