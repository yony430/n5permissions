﻿using Confluent.Kafka;
using N5Permissions.Domain.Contracts;
using N5Permissions.Domain.Models;
using Newtonsoft.Json;

namespace N5Permissions.Infrastructure.Repositories
{
    public class Kafka : IKafka
    {
        private readonly IProducer<Null, string> _producer;

        public Kafka(string url)
        {
            _producer = new ProducerBuilder<Null, string>(new ProducerConfig
            {
                BootstrapServers = url
            }).Build();
        }

        public async Task<bool> CreateEventAsync(Event @event)
        {
            var response = await _producer.ProduceAsync("permissions-topic", new Message<Null, string>
            {
                Value = JsonConvert.SerializeObject(@event)
            });

            return response.Status.Equals(PersistenceStatus.Persisted);
        }
    }
}
