﻿using Microsoft.EntityFrameworkCore;
using N5Permissions.Domain.Contracts;
using N5Permissions.Domain.Models;

namespace N5Permissions.Infrastructure.Repositories
{
    public class DataBaseContext : DbContext, IDataBaseContext
    {

        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options) { }

        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PermissionType> PermissionType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PermissionType>().HasData(
                new PermissionType { Id = 1, Description = "Total Access" },
                new PermissionType { Id = 2, Description = "Edit Access" },
                new PermissionType { Id = 3, Description = "Read0nly Access" }
            );
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
