Pasos para iniciar el proyecto utilizando Docker:

    Ubicarse en la raíz del proyecto (donde se encuentra el Dockerfile y el docker-compose.yml).

    1. Crear la imagen del proyecto API. Para esto, se debe ejecutar el siguiente comando:

        docker build -t docker.n5permissions.co:latest .

    2. Ejecutar el docker-compose.yml:

        docker-compose up

        Esto se encargará de descargar las imágenes requeridas (SQL Server, Kafka, Elasticsearch) y de crear los contenedores necesarios 
        para la ejecución del proyecto.

    3. Correr la aplicación en el puerto 8080:

        http://localhost:8080/swagger/index.html


Nota: En el momento en que se levanta la aplicación por primera vez, se ejecutarán las migraciones necesarias para la creación de la 
      estructura de la base de datos (tablas y relaciones) requeridas. También se insertarán unos campos base para los tipos de permiso (PermissionTypes), los cuales corresponden a:

            1 -> Total Access
            2 -> Edit Access
            3 -> ReadOnly Access

    URL GITLAB : https://gitlab.com/yony430/n5permissions