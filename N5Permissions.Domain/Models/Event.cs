﻿using N5Permissions.Domain.Enums;

namespace N5Permissions.Domain.Models
{
    public class Event
    {
        public Guid Id { get; set; }
        public string OperationName { get; set; }
    }
}
