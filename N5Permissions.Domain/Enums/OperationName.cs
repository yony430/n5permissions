﻿namespace N5Permissions.Domain.Enums
{
    public enum OperationName
    {
        MODIFY,
        GET,
        REQUEST
    }
}
