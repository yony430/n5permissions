﻿namespace N5Permissions.Domain.Exception
{
    public class ElasticSearchException : System.Exception
    {
        public ElasticSearchException()
        {
        }

        public ElasticSearchException(string message)
            : base(message)
        {
        }

        public ElasticSearchException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
