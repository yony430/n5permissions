﻿namespace N5Permissions.Domain.Exception
{
    public class NotPermissionTypeFoundException : System.Exception
    {
        public NotPermissionTypeFoundException()
        {
        }

        public NotPermissionTypeFoundException(string message)
            : base(message)
        {
        }

        public NotPermissionTypeFoundException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
