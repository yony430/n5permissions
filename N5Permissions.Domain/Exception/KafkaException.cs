﻿namespace N5Permissions.Domain.Exception
{
    public class KafkaException : System.Exception
    {
        public KafkaException()
        {
        }

        public KafkaException(string message)
            : base(message)
        {
        }

        public KafkaException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
