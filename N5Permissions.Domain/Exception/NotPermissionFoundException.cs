﻿namespace N5Permissions.Domain.Exception
{
    public class NotPermissionFoundException : System.Exception
    {
        public NotPermissionFoundException()
        {
        }

        public NotPermissionFoundException(string message)
            : base(message)
        {
        }

        public NotPermissionFoundException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
