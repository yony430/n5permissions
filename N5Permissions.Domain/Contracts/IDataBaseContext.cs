﻿using Microsoft.EntityFrameworkCore;
using N5Permissions.Domain.Models;

namespace N5Permissions.Domain.Contracts
{
    public interface IDataBaseContext
    {
        DbSet<Permission> Permissions { get; set; }
        DbSet<PermissionType> PermissionType { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
