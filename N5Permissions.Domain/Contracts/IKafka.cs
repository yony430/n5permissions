﻿using N5Permissions.Domain.Models;

namespace N5Permissions.Domain.Contracts
{
    public interface IKafka
    {
        Task<bool> CreateEventAsync(Event @event);
    }
}
