﻿using N5Permissions.Domain.Models;

namespace N5Permissions.Domain.Contracts
{
    public interface IElasticSearch
    {
        Task<bool> CreateIndexAsync(Permission permission);
        Task<bool> ModifyIndexAsync(Permission permission);
        Task<IEnumerable<Permission>> GetAsync();
    }
}
