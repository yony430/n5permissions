﻿using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Moq;
using N5Permissions.Business.Services;
using N5Permissions.Controllers;
using N5Permissions.Domain.Models;
using N5Permissions.Models.Request;
using N5Permissions.Test.Helpers;

namespace N5Permissions.Test.Controllers
{
    public class PermissionsControllerTests
    {
        [Fact]
        public async Task Request_WithValidPermission_ReturnsOkResult()
        {
            // Arrange
            var mockPermissionService = new Mock<IPermissionService>();
            var controller = new PermissionsController(mockPermissionService.Object);
            var permissionDTO = N5PermissionsHelperTest.GetPermissionDTO();
            var validationResult = new ValidationResult();

            mockPermissionService.Setup(service => service.RequestAsync(It.IsAny<Permission>()))
                                  .ReturnsAsync(N5PermissionsHelperTest.GetPermissionData());
            var mapper = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<PermissionDTO, Permission>()));
            validationResult.Errors.AddRange(new List<ValidationFailure>());

            // Act
            var result = await controller.Request(permissionDTO);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var createdPermission = Assert.IsType<Permission>(okResult.Value);
        }

        [Fact]
        public async Task Get_ReturnsOkResultWithData()
        {
            // Arrange
            var mockPermissionService = new Mock<IPermissionService>();
            var controller = new PermissionsController(mockPermissionService.Object);
            var permissions = N5PermissionsHelperTest.GetPermissions();

            mockPermissionService.Setup(service => service.GetAsync()).ReturnsAsync(permissions);

            // Act
            var result = await controller.Get();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnedPermissions = Assert.IsAssignableFrom<IEnumerable<Permission>>(okResult.Value);
            Assert.Equal(5, returnedPermissions.Count());
        }

        [Fact]
        public async Task Modify_WithValidIdAndPermission_ReturnsOkResult()
        {
            // Arrange
            var mockPermissionService = new Mock<IPermissionService>();
            var controller = new PermissionsController(mockPermissionService.Object);
            var permissionId = 1; 
            var permissionDTO = N5PermissionsHelperTest.GetPermissionDTO();
            var validationResult = new ValidationResult();

            mockPermissionService.Setup(service => service.ModifyAsync(permissionId, It.IsAny<Permission>()))
                                  .ReturnsAsync(N5PermissionsHelperTest.GetPermissionData());
            var mapper = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<PermissionDTO, Permission>()));
            validationResult.Errors.AddRange(new List<ValidationFailure>());

            // Act
            var result = await controller.Modify(permissionId, permissionDTO);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<Permission>(okResult.Value);
        }
    }
}
