﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Moq;
using N5Permissions.Business.Services.Implementations;
using N5Permissions.Domain.Contracts;
using N5Permissions.Domain.Enums;
using N5Permissions.Domain.Exception;
using N5Permissions.Domain.Models;
using N5Permissions.Test.Helpers;

namespace N5Permissions.Test.Services
{
    public class PermissionServiceTest
    {
        [Fact]
        public async Task Request_Permission_Type_Not_Valid_Async()
        {
            // Arrange
            var dbContextMock = new Mock<IDataBaseContext>();
            var elasticSearchMock = new Mock<IElasticSearch>();
            var kafkaMock = new Mock<IKafka>();
            var permission = N5PermissionsHelperTest.GetEmptyPermissionData();

            dbContextMock.Setup(mock => mock.PermissionType.FindAsync(It.IsAny<int>)).ReturnsAsync((PermissionType)null);

            var service = new PermissionService(dbContextMock.Object, elasticSearchMock.Object, kafkaMock.Object);

            // Act
            var ex = await Assert.ThrowsAsync<NotPermissionTypeFoundException>(() => service.RequestAsync(permission));

            // Assert
            Assert.Equal($"El tipo de permiso con id {permission.PermissionTypeId} no existe.", ex.Message);
        }

        [Fact]
        public async Task Request_Permission_Failed_Elasticsearch_Async()
        {
            // Arrange
            var dbContextMock = new Mock<IDataBaseContext>();
            var elasticSearchMock = new Mock<IElasticSearch>();
            var kafkaMock = new Mock<IKafka>();
            var permission = N5PermissionsHelperTest.GetEmptyPermissionData();

            dbContextMock.Setup(mock => mock.PermissionType.FindAsync(permission.PermissionTypeId)).ReturnsAsync(N5PermissionsHelperTest.GetPermissionType());

            dbContextMock.Setup(_ => _.Permissions.AddAsync(It.IsAny<Permission>(), It.IsAny<CancellationToken>()))
                 .Callback((Permission model, CancellationToken token) => { })
                 .ReturnsAsync((EntityEntry<Permission>)null);

            dbContextMock.Setup(c => c.SaveChangesAsync(default)).Returns(Task.FromResult(1)).Verifiable();
            elasticSearchMock.Setup(mock => mock.CreateIndexAsync(permission)).ReturnsAsync(false);

            var service = new PermissionService(dbContextMock.Object, elasticSearchMock.Object, kafkaMock.Object);

            // Act
            var ex = await Assert.ThrowsAsync<ElasticSearchException>(() => service.RequestAsync(permission));

            // Assert
            Assert.Equal("Error creando indice en elastic search.", ex.Message);
        }

        [Fact]
        public async Task Request_Permission_Failed_kakfa_Async()
        {
            // Arrange
            var dbContextMock = new Mock<IDataBaseContext>();
            var elasticSearchMock = new Mock<IElasticSearch>();
            var kafkaMock = new Mock<IKafka>();
            var permission = N5PermissionsHelperTest.GetEmptyPermissionData();
            var eventData = N5PermissionsHelperTest.GetEvent(OperationName.REQUEST);

            dbContextMock.Setup(mock => mock.PermissionType.FindAsync(permission.PermissionTypeId)).ReturnsAsync(N5PermissionsHelperTest.GetPermissionType());
            dbContextMock.Setup(_ => _.Permissions.AddAsync(It.IsAny<Permission>(), It.IsAny<CancellationToken>()))
                 .Callback((Permission model, CancellationToken token) => { })
                 .ReturnsAsync((EntityEntry<Permission>)null);

            dbContextMock.Setup(c => c.SaveChangesAsync(default)).Returns(Task.FromResult(1)).Verifiable();

            elasticSearchMock.Setup(mock => mock.CreateIndexAsync(permission)).ReturnsAsync(true);
            kafkaMock.Setup(mock => mock.CreateEventAsync(eventData)).ReturnsAsync(false);

            var service = new PermissionService(dbContextMock.Object, elasticSearchMock.Object, kafkaMock.Object);

            // Act
            var ex = await Assert.ThrowsAsync<KafkaException>(() => service.RequestAsync(permission));

            // Assert
            Assert.Equal("Error publicando evento en kafka.", ex.Message);
        }

        [Fact]
        public async Task Request_Permission_Valid_Async()
        {
            // Arrange
            var dbContextMock = new Mock<IDataBaseContext>();
            var elasticSearchMock = new Mock<IElasticSearch>();
            var kafkaMock = new Mock<IKafka>();
            var permission = N5PermissionsHelperTest.GetEmptyPermissionData();
            var eventData = N5PermissionsHelperTest.GetEvent(OperationName.REQUEST);

            dbContextMock.Setup(mock => mock.PermissionType.FindAsync(permission.PermissionTypeId)).ReturnsAsync(N5PermissionsHelperTest.GetPermissionType());
            dbContextMock.Setup(_ => _.Permissions.AddAsync(It.IsAny<Permission>(), It.IsAny<CancellationToken>()))
                 .Callback((Permission model, CancellationToken token) => { })
                 .ReturnsAsync((EntityEntry<Permission>)null);

            dbContextMock.Setup(c => c.SaveChangesAsync(default)).Returns(Task.FromResult(1)).Verifiable();

            elasticSearchMock.Setup(mock => mock.CreateIndexAsync(permission)).ReturnsAsync(true);
            kafkaMock.Setup(mock => mock.CreateEventAsync(It.IsAny<Event>())).ReturnsAsync(true);

            var service = new PermissionService(dbContextMock.Object, elasticSearchMock.Object, kafkaMock.Object);

            // Act
            var result = await service.RequestAsync(permission);

            // Assert
            Assert.NotNull(result);
        }
    }
}
