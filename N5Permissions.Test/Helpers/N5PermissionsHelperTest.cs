﻿using N5Permissions.Domain.Enums;
using N5Permissions.Domain.Models;
using N5Permissions.Models.Request;
using System;

namespace N5Permissions.Test.Helpers
{
    public static class N5PermissionsHelperTest
    {
        public static Permission GetEmptyPermissionData()
        {
            return new Permission()
            {
                EmployeeForename = string.Empty,
                EmployeeSurname = string.Empty,
                PermissionTypeId = 7
            };
        }

        public static Permission GetPermissionData()
        {
            Random random = new Random();

            return new Permission()
            {
                Id = random.Next(1, 100),
                EmployeeForename = "Pepe",
                EmployeeSurname = "Perez",
                PermissionTypeId = 1
            };
        }

        public static IEnumerable<Permission> GetPermissions()
        {
            return new List<Permission>()
            {
                GetEmptyPermissionData(),
                GetEmptyPermissionData(),
                GetEmptyPermissionData(),
                GetEmptyPermissionData(),
                GetEmptyPermissionData(),
            };
        }

        public static PermissionType GetPermissionType()
        {
            Random random = new Random();

            return new PermissionType()
            {
                Description = string.Empty,
                Id = random.Next(1, 10)
            };
        }

        public static Event GetEvent(OperationName operationName)
        {
            return new Event()
            {
                Id = Guid.NewGuid(),
                OperationName = operationName.ToString()
            };
        }

        public static PermissionDTO GetPermissionDTO()
        {
            return new PermissionDTO()
            {
                PermissionTypeId = 1,
                EmployeeForename = "Pepe",
                EmployeeSurname = "Perez"
            };
        }
    }
}
