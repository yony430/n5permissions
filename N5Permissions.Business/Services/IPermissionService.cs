﻿using N5Permissions.Domain.Models;

namespace N5Permissions.Business.Services
{
    public interface IPermissionService
    {
        Task<Permission> RequestAsync(Permission permission);
        Task<Permission> ModifyAsync(int id, Permission permission);
        Task<IEnumerable<Permission>> GetAsync();
    }
}
