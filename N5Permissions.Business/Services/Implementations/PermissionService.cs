﻿using N5Permissions.Domain.Contracts;
using N5Permissions.Domain.Enums;
using N5Permissions.Domain.Exception;
using N5Permissions.Domain.Models;
using Serilog;

namespace N5Permissions.Business.Services.Implementations
{
    public class PermissionService : IPermissionService
    {
        private readonly IDataBaseContext _dbContext;
        private readonly IElasticSearch _elasticSearch;
        private readonly IKafka _kafka;


        public PermissionService(IDataBaseContext dbContext,
            IElasticSearch elasticSearch,
            IKafka kafka
            )
        {
            _dbContext = dbContext;
            _elasticSearch = elasticSearch;
            _kafka = kafka;
        }

        public async Task<IEnumerable<Permission>> GetAsync()
        {
            Log.Information("Inicio del proceso para obtener los permisos desde elasticsearch ...");
            var permissions = await _elasticSearch.GetAsync();

            Log.Information("Publicando evento en kafka.");
            await PublicKafkaEventAsync(OperationName.GET);

            Log.Information("Fin del proceso de obtención de permisos.");

            return permissions;
        }

        public async Task<Permission> ModifyAsync(int id, Permission permissionNewData)
        {
            Log.Information("Inicio del proceso para la modificación de un permisos ...");
            
            Log.Information("Validando que el permiso a editar exista en base de datos SQL...");
            var permission = await _dbContext.Permissions.FindAsync(id);

            if(permission == null)
                throw new NotPermissionFoundException($"No se encontro información del permiso {id}.");

            Log.Information("Validando que el tipo de permiso ingresado sea valido.");
            var existPermissionType = await ExistPermissionType(permissionNewData.PermissionTypeId);

            if (!existPermissionType)
                throw new NotPermissionTypeFoundException($"El tipo de permiso con id {permissionNewData.PermissionTypeId} no existe.");

            Log.Information("Actualizando permiso en base de datos de SQL.");
            permission.EmployeeForename = permissionNewData.EmployeeForename;
            permission.EmployeeSurname = permissionNewData.EmployeeSurname;
            permission.PermissionTypeId = permissionNewData.PermissionTypeId;
            permission.Date = DateTime.Now;
            await _dbContext.SaveChangesAsync();

            Log.Information("Actualizando indice en elasticsearch.");
            await _elasticSearch.ModifyIndexAsync(permission);

            Log.Information("Publicando evento en kafka.");
            await PublicKafkaEventAsync(OperationName.MODIFY);

            Log.Information("Fin del proceso de modificación de permiso.");

            return permission;
        }

        public async Task<Permission> RequestAsync(Permission permission)
        {
            Log.Information("Inicio del proceso para solicitud de permiso...");

            Log.Information("Validando que el tipo de permiso ingresado sea valido.");
            var existPermissionType = await ExistPermissionType(permission.PermissionTypeId);

            if (!existPermissionType)
                throw new NotPermissionTypeFoundException($"El tipo de permiso con id {permission.PermissionTypeId} no existe.");

            Log.Information("Creando el permiso en base de datos de SQL.");
            permission.Date = DateTime.Now;
            await _dbContext.Permissions.AddAsync(permission);
            await _dbContext.SaveChangesAsync();

            Log.Information("Ingresando indice en elasticsearch.");
            await CreateElasticSearchIndexAsync(permission);

            Log.Information("Publicando evento en kafka.");
            await PublicKafkaEventAsync(OperationName.REQUEST);

            Log.Information("Fin del proceso de solicitud de permiso.");

            return permission;
        }

        private async Task<bool> CreateElasticSearchIndexAsync(Permission permission)
        {
            var response = await _elasticSearch.CreateIndexAsync(permission);

            if (!response)
            {
                throw new ElasticSearchException("Error creando indice en elastic search.");
            }

            return response;
        }

        private async Task<bool> PublicKafkaEventAsync(OperationName operationName)
        {
            var response = await _kafka.CreateEventAsync(new Event() { Id = Guid.NewGuid(), OperationName = operationName.ToString() });

            if (!response)
            {
                throw new KafkaException("Error publicando evento en kafka.");
            }

            return response;
        }

        private async Task<bool> ExistPermissionType(int permissionTypeId)
        {
            var permissionType = await _dbContext.PermissionType.FindAsync(permissionTypeId);
            return permissionType != null;
        }
    }
}
